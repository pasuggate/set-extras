{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}
module Data.Bits.Sets
       ( module Math.Sets
       ) where

------------------------------------------------------------------------------
--
--  Functions that treat strings of bits as sets.
--
--  Changelog:
--   + 20/06/2015  --  initial file;
--
--  TODO:
--   + testing, and optimisation;
--   + decide on constraints and `SetElems` types;
--

-- import GHC.Word
import Data.Bits
import Math.Sets


-- * For @Int type
------------------------------------------------------------------------------
instance SetOps Int where
  (/\) = (.&.)
  (\/) = (.|.)
  a\\b = a .&. complement b

instance SetSize Int where
  dry  = (0 ==)
  card = popCount
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetInsert Int Int where
  (~>) = flip setBit
  {-# INLINE (~>) #-}

instance SetIndex Int Int where
  (<?) = flip testBit
  (<\) = flip clearBit
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild Int Int where
  bare = 0
  unit = setBit 0
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetElems Int Int Int where
  i<~a = if i<?a then i else 0
  {-# INLINE (<~) #-}


-- * For @Word type
------------------------------------------------------------------------------
instance SetOps Word where
  (/\) = (.&.)
  (\/) = (.|.)
  a\\b = a .&. complement b

instance SetSize Word where
  dry  = (0 ==)
  card = popCount
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetInsert Int Word where
  (~>) = flip setBit
  {-# INLINE (~>) #-}

instance SetIndex Int Word where
  (<\) = flip clearBit
  (<?) = flip testBit
  {-# INLINE (<?) #-}
  {-# INLINE (<\) #-}

instance SetBuild Int Word where
  bare = 0
  unit = setBit 0
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetElems Int Int Word where
  i<~a = if i<?a then i else 0
  {-# INLINE (<~) #-}
