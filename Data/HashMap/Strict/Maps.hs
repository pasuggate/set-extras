{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, Strict #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.HashMap.Strict.Maps
-- Copyright   : (C) Patrick Suggate 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Defines some operators for fairly standard and useful 'HashMap' functions.
--
-- Changelog:
--  + 02/09/2020  --  initial file;
--
------------------------------------------------------------------------------

module Data.HashMap.Strict.Maps
  ( module Math.Sets
  , HashMap
  )
where

import           Data.Hashable       (Hashable)
import           Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as Map
import           Math.Sets


-- * Orphan instances
------------------------------------------------------------------------------
instance SetSize (HashMap k a) where
  dry  = Map.null
  card = Map.size
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetFlat (k,v) (HashMap k v) where
  flat = Map.toList
  {-# INLINE flat #-}

instance Hashable k => SetBuild (k,v) (HashMap k v) where
  bare = Map.empty
  unit = uncurry Map.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

------------------------------------------------------------------------------
instance (Eq k, Hashable k) => SetInsert (k,v) (HashMap k v) where
  (k, v) ~> m = Map.insert k v m
  {-# INLINE (~>) #-}

instance (Eq k, Hashable k) => SetIndex k (HashMap k v) where
  (<\) = Map.delete
  (<?) = Map.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance (Eq k, Hashable k) => SetElems k (Maybe v) (HashMap k v) where
  (<~) = Map.lookup
  {-# INLINE (<~) #-}

------------------------------------------------------------------------------
instance (Eq k, Hashable k) => SetOps (HashMap k e) where
  (/\) = Map.intersection
  (\/) = Map.union
  (\\) = Map.difference
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}
