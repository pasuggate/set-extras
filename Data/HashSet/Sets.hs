{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.HashSet.Sets
-- Copyright   : (C) Patrick Suggate 2020
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Defines some operators for fairly standard and useful 'HashSet' functions.
--
-- Changelog:
--  + 02/09/2020  --  initial file;
--
------------------------------------------------------------------------------

module Data.HashSet.Sets
  ( module Math.Sets
  , HashSet
  )
where

import           Data.Hashable (Hashable)
import           Data.HashSet  (HashSet)
import qualified Data.HashSet  as Set
import           Math.Sets


-- * Orphan instances
------------------------------------------------------------------------------
instance SetSize (HashSet a) where
  dry  = Set.null
  card = Set.size
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetFlat a (HashSet a) where
  flat = Set.toList
  {-# INLINE flat #-}

instance Hashable a => SetBuild a (HashSet a) where
  bare = Set.empty
  unit = Set.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

------------------------------------------------------------------------------
instance (Eq a, Hashable a) => SetInsert a (HashSet a) where
  (~>) = Set.insert
  {-# INLINE (~>) #-}

instance (Eq a, Hashable a) => SetIndex a (HashSet a) where
  (<\) = Set.delete
  (<?) = Set.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

------------------------------------------------------------------------------
instance (Eq a, Hashable a) => SetOps (HashSet a) where
  (/\) = Set.intersection
  (\/) = Set.union
  (\\) = Set.difference
  {-# INLINABLE (/\) #-}
  {-# INLINABLE (\/) #-}
  {-# INLINABLE (\\) #-}
