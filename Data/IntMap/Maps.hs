{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.IntMap.Maps
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Defines some fairly standard and useful 'IntMap' operators.
-- 
------------------------------------------------------------------------------

module Data.IntMap.Maps
  ( module Math.Sets
  , IntMap
  )
where

import           Data.IntMap                    ( IntMap )
import qualified Data.IntMap                   as Map
import           Data.MonoTraversable           ( )

import           Math.Sets


instance SetOps (IntMap e) where
  (/\) = Map.intersection
  (\/) = Map.union
  (\\) = Map.difference
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}

-- * Orphan instances
------------------------------------------------------------------------------
-- | Inferred from the 'MonoFoldable' instance.
instance SetSize (IntMap e)

instance SetInsert (Int,v) (IntMap v) where
  (k, v) ~> m = Map.insert k v m
  {-# INLINE (~>) #-}

instance SetIndex Int (IntMap v) where
  (<\) = Map.delete
  (<?) = Map.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance SetBuild (Int,v) (IntMap v) where
  bare = Map.empty
  unit = uncurry Map.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetElems Int (Maybe v) (IntMap v) where
  (<~) = Map.lookup
  {-# INLINE (<~) #-}

instance SetFlat (Int,v) (IntMap v) where
  flat = Map.toList
  {-# INLINE flat #-}
