{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Data.IntMap.Poset
       ( module Math.Sets
       , IntSet
       , Poset
       ) where

------------------------------------------------------------------------------
--
--  TODO: Because the type: 
--
--           type Poset = IntMap IntSet
--
--    has a `Data.Monoid' element, more specifically `mempty', then queries
--    that return `Maybe a' types can be simplified.
--    So, what I want is separate instances for when the element-type has the
--    monoid properties; and for non-monoids.
--

import Data.IntMap (IntMap)

import Math.Sets
import Data.IntSet.Sets
import Data.IntMap.Maps ()

type Poset = IntMap IntSet


{-

instance SetOps (IntMap IntSet) where
  {-# INLINABLE (/\) #-}
  {-# INLINABLE (\/) #-}
  {-# INLINABLE (\\) #-}
  {-# INLINABLE null #-}
  {-# INLINABLE card #-}
  (/\) = Map.intersection
  (\/) = Map.union
  (\\) = Map.difference
  null = Map.null
  card = Map.size

instance SetInsert (Int,IntSet) Poset where
  {-# INLINABLE (~>) #-}
  (k,v) ~> m = Map.insert k v m

instance SetIndex Int Poset where
  {-# INLINABLE (<\) #-}
  {-# INLINABLE (<?) #-}
  (<\) = Map.delete
  (<?) = Map.member

instance SetBuild (Int,IntSet) Poset where
  {-# INLINABLE bare #-}
  {-# INLINABLE unit #-}
  bare = Map.empty
  unit = uncurry Map.singleton

instance SetElems Int (Maybe IntSet) Poset where
  {-# INLINABLE (<~) #-}
  (<~) = Map.lookup

{-
instance SetElems Int IntSet Poset where
  {-# INLINABLE (<~) #-}
  (<~) i = maybe bare id . Map.lookup i
-}

instance SetFlat (Int,IntSet) Poset where
  {-# INLINABLE flat #-}
  flat = Map.toList
-}
