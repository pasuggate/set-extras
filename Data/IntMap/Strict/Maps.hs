{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.IntMap.Strict.Maps
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Defines some fairly standard and useful (strict) 'IntMap' operators.
-- 
------------------------------------------------------------------------------

module Data.IntMap.Strict.Maps
  ( module Math.Sets
  , IntMap
  )
where

import           Data.IntMap.Strict             ( IntMap )
import qualified Data.IntMap.Strict            as Map

import           Math.Sets


instance SetOps (IntMap e) where
  (/\) = Map.intersection
  (\/) = Map.union
  (\\) = Map.difference
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}

-- NOTE: does not use the mono-traversable instance, to ensure that strictness
--   property is retained.
-- TODO: check 'MonoFoldable' instance and see if it can be used.
instance SetSize (IntMap e) where
  dry  = Map.null
  card = Map.size
  {-# INLINE dry #-}
  {-# INLINE card #-}

instance SetInsert (Int,v) (IntMap v) where
  (k, v) ~> m = Map.insert k v m
  {-# INLINE (~>) #-}

instance SetIndex Int (IntMap v) where
  (<\) = Map.delete
  (<?) = Map.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance SetBuild (Int,v) (IntMap v) where
  bare = Map.empty
  unit = uncurry Map.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetElems Int (Maybe v) (IntMap v) where
  (<~) = Map.lookup
  {-# INLINE (<~) #-}

instance SetFlat (Int,v) (IntMap v) where
  flat = Map.toList
  {-# INLINE flat #-}
