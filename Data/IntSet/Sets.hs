{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.IntSet.Sets
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Defines some fairly standard and useful 'IntSet' operators.
-- 
------------------------------------------------------------------------------

module Data.IntSet.Sets
  ( module Math.Sets
  , IntSet
  )
where

import           Prelude                 hiding ( null )
import           Data.IntSet                    ( IntSet )
import qualified Data.IntSet                   as IntSet
import           Data.MonoTraversable           ( )

import           Math.Sets


-- * Orphan instances
------------------------------------------------------------------------------
-- | Inferred from the 'MonoFoldable' instance.
instance SetSize IntSet

-- | Inferred from the 'MonoFoldable' instance.
instance SetFlat Int IntSet

------------------------------------------------------------------------------
instance SetInsert Int IntSet where
  (~>) = IntSet.insert
  {-# INLINE (~>) #-}

instance SetElems Int (Maybe Int) IntSet where
  j <~ js = if j <? js then Just j else Nothing
  {-# INLINE (<~) #-}

instance SetIndex Int IntSet where
  (<\) = IntSet.delete
  (<?) = IntSet.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance SetBuild Int IntSet where
  bare = IntSet.empty
  unit = IntSet.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetOps IntSet where
  (/\) = IntSet.intersection
  (\/) = IntSet.union
  (\\) = (IntSet.\\)
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}
