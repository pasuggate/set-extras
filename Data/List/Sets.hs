{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}
module Data.List.Sets
       ( module Math.Sets
       ) where

import qualified Data.List as List
import Math.Sets
import Data.MonoTraversable ()


-- * Orphan instances
------------------------------------------------------------------------------
-- | Inferred from the 'MonoFoldable' instance.
instance SetSize [a]

------------------------------------------------------------------------------
-- TODO: Should the constraint on the free type require that elements be
--   "ordered sets," so that `Set's of `Set's (and any other set-like
--   elements) can be used, and in a generic way?
instance Eq a => SetOps [a] where
  (/\) = List.intersect
  (\/) = List.union
  (\\) = (List.\\)
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}

instance SetInsert a [a] where
  (~>) = (:)
  {-# INLINE (~>) #-}

instance Eq a => SetIndex a [a] where
  (<\) = List.delete
  (<?) = List.elem
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance SetBuild a [a] where
  bare = []
  unit = (:[])
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetFlat a [a] where
  flat = id
  {-# INLINE flat #-}
