{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Map.Maps
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Defines some fairly standard and useful 'Map' operators.
--
------------------------------------------------------------------------------

module Data.Map.Maps
  ( module Math.Sets
  , Map
  )
where

import           Data.Map             (Map)
import qualified Data.Map             as Map
import           Data.MonoTraversable ()

import           Math.Sets


-- * Orphan instances
------------------------------------------------------------------------------
-- | The default instance uses 'MonoFoldable', to which 'Map' belongs.
instance Ord k => SetSize (Map k a)

instance Ord k => SetInsert (k,v) (Map k v) where
  {-# INLINE (~>) #-}
  (k, v) ~> m = Map.insert k v m

instance Ord k => SetIndex k (Map k v) where
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}
  (<\) = Map.delete
  (<?) = Map.member

instance Ord k => SetBuild (k,v) (Map k v) where
  {-# INLINE bare #-}
  {-# INLINE unit #-}
  bare = Map.empty
  unit = uncurry Map.singleton

instance Ord k => SetElems k (Maybe v) (Map k v) where
  {-# INLINE (<~) #-}
  (<~) = Map.lookup

instance Ord k => SetFlat (k,v) (Map k v) where
  {-# INLINE flat #-}
  flat = Map.toList

instance Ord k => SetOps (Map k e) where
  (/\) = Map.intersection
  (\/) = Map.union
  (\\) = Map.difference
  {-# INLINE (/\) #-}
  {-# INLINE (\/) #-}
  {-# INLINE (\\) #-}
