{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Data.MultiMap
       ( -- Constructors
         MultiMap(..)
       , MMType
         -- Modifiers
       , (<\\)
         -- Queries
       , findMin, findMax
       ) where

------------------------------------------------------------------------------
--
--  A basic (and `Int'-only) multimap library.
--

import Prelude hiding (head,tail,span,null)
import Control.Arrow ((***),(&&&),(<<<))
import qualified Data.IntSet as IntSet
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap

import Data.IntSet.Sets
import Data.IntMap.Poset ()


type    MMType   = IntMap IntSet
newtype MultiMap = Multi { multi :: MMType }
                 deriving (Eq,Show)


instance SetBuild (Int,IntSet) MultiMap where
  bare = Multi bare
  unit = Multi . unit
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance SetOps MultiMap where
  (/\) = binOp (IntMap.intersectionWith (/\))
  (\/) = binOp (IntMap.unionWith (\/))
  (\\) = binOp (IntMap.differenceWith (curry (Just . uncurry (\\))))
  {-# INLINABLE (/\) #-}
  {-# INLINABLE (\/) #-}
  {-# INLINABLE (\\) #-}

instance SetSize MultiMap where
  dry  = dry  . multi
  card = card . multi
  {-# INLINE dry  #-}
  {-# INLINE card #-}

instance SetIndex (Int,Int) MultiMap where
  (<?) (j,k) = (k<?) . (j<~)
  (<\) (j,k) = Multi <<< uncurry ins <<< (,) j . (k<\) . (j<~) &&& (j<\) . multi
  {-# INLINE    (<?) #-}
  {-# INLINABLE (<\) #-}

instance SetInsert (Int,Int) MultiMap where
  (~>) (j,k) = Multi <<< uncurry (~>) <<< (,) j . (k~>) . (j<~) &&& multi
  {-# INLINABLE (~>) #-}

instance SetElems Int IntSet MultiMap where
  (<~) j = maybe bare id . (j<~) . multi
  {-# INLINE (<~) #-}

-- TODO: Generate full edge-list?
instance SetFlat (Int,IntSet) MultiMap where
  flat = flat . multi
  {-# INLINE flat #-}


-- * Modifiers.
------------------------------------------------------------------------------
-- | Delete all elements at the given index.
(<\\) :: Int -> MultiMap -> MultiMap
(<\\) j = Multi . (j<\) . multi


-- * Queries.
findMin :: MultiMap -> Int
findMin = IntSet.findMin . snd . IntMap.findMin . multi

findMax :: MultiMap -> Int
findMax = IntSet.findMin . snd . IntMap.findMax . multi


-- * Internal functions.
binOp :: (MMType -> MMType -> MMType) -> MultiMap -> MultiMap -> MultiMap
binOp f = curry (Multi . uncurry f <<< multi *** multi)

ins :: (Int,IntSet) -> MMType -> MMType
ins (j,js) m | dry  js   = m
             | otherwise = (j,js)~>m
