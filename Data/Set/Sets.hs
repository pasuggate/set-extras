{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Set.Sets
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Defines some fairly standard and useful 'Set' operators.
--
------------------------------------------------------------------------------

module Data.Set.Sets
  ( module Math.Sets
  , Set
  )
where

import           Data.MonoTraversable ()
import           Data.Set             (Set)
import qualified Data.Set             as Set

import           Math.Sets


-- * Orphan instances
------------------------------------------------------------------------------
-- | The default instance uses 'MonoFoldable', to which 'Set' belongs.
instance Ord a => SetSize (Set a)

instance Ord a => SetFlat a (Set a)

------------------------------------------------------------------------------
instance Ord a => SetInsert a (Set a) where
  (~>) = Set.insert
  {-# INLINE (~>) #-}

instance Ord a => SetIndex a (Set a) where
  (<\) = Set.delete
  (<?) = Set.member
  {-# INLINE (<\) #-}
  {-# INLINE (<?) #-}

instance Ord a => SetBuild a (Set a) where
  bare = Set.empty
  unit = Set.singleton
  {-# INLINE bare #-}
  {-# INLINE unit #-}

instance Ord a => SetOps (Set a) where
  {-# INLINABLE (/\) #-}
  {-# INLINABLE (\/) #-}
  {-# INLINABLE (\\) #-}
  (/\) = Set.intersection
  (\/) = Set.union
  (\\) = Set.difference
