module Data.Tree.Size where

------------------------------------------------------------------------------
--
--  Size-trees.
--

import Prelude hiding (drop,null)
import Control.Arrow ((&&&))
import qualified Data.List as List
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap


------------------------------------------------------------------------------
-- | An `IntMap' of sizes and elements, where a monoid is formed for elements 
--   of each size.
--   TODO: Parameterise by monoid-type? (`Set's would be nice?)
--   TODO: `Data.Sequence' is strict, would this be desirable?
newtype SizeTree a = ST { unpack :: IntMap [a] }
                   deriving (Eq,Show)


-- * Initialisation functions.
------------------------------------------------------------------------------
-- | New and empty SizeTree.
empty :: SizeTree a
empty = ST $ IntMap.empty

------------------------------------------------------------------------------
-- | Singleton SizeTree.
singleton :: Int -> a -> SizeTree a
singleton k = ST . IntMap.singleton k . (:[])


-- * Conversions.
------------------------------------------------------------------------------
-- | Flatten to a list.
toList :: SizeTree a -> [a]
toList = concat . IntMap.elems . unpack

sizeList :: SizeTree a -> [Int]
sizeList = concatMap (\(s,xs) -> replicate (length xs) s) .
           IntMap.toList . unpack

size :: SizeTree a -> Int
size = length . sizeList


-- * Basic queries.
------------------------------------------------------------------------------
-- | Empty?
null :: SizeTree a -> Bool
null (ST ts) = IntMap.null ts


-- * Modifcation functions.
------------------------------------------------------------------------------
-- | Insert an entry.
--   O(log n)
-- NOTE: No duplicate-checking is performed.
ins :: Int -> a -> SizeTree a -> SizeTree a
ins s x = uncurry (ins' s x) . ((get' s) &&& id) . unpack

------------------------------------------------------------------------------
-- | Insert an entry if it passes the required check.
--   O(log n)
-- NOTE: No duplicate-checking is performed.
insIf :: ([a] -> Bool) -> Int -> a -> SizeTree a -> SizeTree a
insIf f s x (ST ts) = if f xs then ins' s x xs ts else ST ts
  where xs = get' s ts

------------------------------------------------------------------------------
-- | Delete an entry.
--   O(n)
-- TODO: Is returning the tree "unmolested" the best failure mode?
del :: Eq a => Int -> a -> SizeTree a -> SizeTree a
del s x (ST ts) = case IntMap.lookup s ts of
  Nothing -> ST ts
  Just xs -> ST ts'
    where ts' = if List.null xs'
                then IntMap.delete s ts
                else IntMap.insert s xs' ts
          xs' = List.delete x xs

------------------------------------------------------------------------------
-- | Drop all elems of the given size.
--   O(log n)
drop :: Int -> SizeTree a -> SizeTree a
drop s (ST ts) = ST $ IntMap.delete s ts

------------------------------------------------------------------------------
-- | Drop all elems of the given size and below.
--   O(n)
dropFrom :: Int -> SizeTree a -> SizeTree a
dropFrom s (ST ts) = ST $ IntMap.filterWithKey (\k _ -> k>s) ts

------------------------------------------------------------------------------
-- | Drop any existing elems and insert the given elem.
--   O(log n)
replace :: Int -> a -> SizeTree a -> SizeTree a
replace s x = ins s x . drop s


-- * Max/min functions.
------------------------------------------------------------------------------
-- | Find the first element with the maximum size, and remove it from the
--   size-tree, then return both the element and the updated size-tree.
--   O(log n)
getMax :: SizeTree a -> Maybe (a,SizeTree a)
getMax = upd' . IntMap.maxViewWithKey . unpack

------------------------------------------------------------------------------
-- | Find the first element with the minimum size, and remove it from the
--   size-tree, then return both the element and the updated size-tree.
--   O(log n)
getMin :: SizeTree a -> Maybe (a,SizeTree a)
getMin = upd' . IntMap.minViewWithKey . unpack


-- * Internal helper functions.
upd' :: Maybe ((Int,[a]),IntMap [a]) -> Maybe (a,SizeTree a)
upd'  Nothing             = Nothing
upd' (Just ((k,x:[]),ts)) = Just (x,ST $ IntMap.delete k    ts)
upd' (Just ((k,x:xs),ts)) = Just (x,ST $ IntMap.insert k xs ts)

ins' :: Int -> a -> [a] -> IntMap [a] -> SizeTree a
ins' k x xs = ST . IntMap.insert k (x:xs)

get' :: Int -> IntMap [a] -> [a]
get' k ts = case IntMap.lookup k ts of
  Nothing -> []
  Just xs -> xs
