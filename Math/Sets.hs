{-# LANGUAGE DefaultSignatures, FunctionalDependencies, TypeFamilies #-}

------------------------------------------------------------------------------
-- |
-- Module      : Math.Sets
-- Copyright   : (C) Patrick Suggate 2011
-- License     : BSD3
--
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Defines some fairly standard and useful set operators.
--
-- == TODO:
--  - determine the correct fixities for the operators;
--  - refactor the classes:
--    * '<?' and '<\' shouldn't be in the same class?
--    * 'bare' and 'unit' shouldn't be in the 'SetBuild' class;
--    * use 'mono-traversable' whenever possible?
--
------------------------------------------------------------------------------

module Math.Sets where

import           Data.Foldable        (foldl')
import           Data.MonoTraversable (Element, MonoFoldable (..),
                                       MonoPointed (..))
import           Data.Sequences       (Index, IsSequence (..))


-- * Type-classes for many set-type operations.
------------------------------------------------------------------------------
-- | Operations for some standard set operations:
--
-- - intersection '(/\\)'
-- - union '(\\/)'
-- - difference '(\\\\)'
--
class SetOps a where
  (/\) :: a -> a -> a
  (\/) :: a -> a -> a
  (\\) :: a -> a -> a

infixl 6 /\
infixl 5 \/,\\

-- | Query a set-like data-structure for its cardinality, or emptiness.
class SetSize a where
  dry  :: a -> Bool
  card :: a -> Int
  default dry  :: MonoFoldable a => a -> Bool
  dry  = onull
  {-# INLINE dry  #-}
  default card :: MonoFoldable a => a -> Int
  card = olength
  {-# INLINE card #-}

-- | Obviously ...
wet :: SetSize a => a -> Bool
wet  = not . dry
{-# INLINE wet #-}

------------------------------------------------------------------------------
-- | Construct a new set by inserting a new element into an existing set.
class SetInsert e s | s -> e where
  (~>) :: e -> s -> s

infixr 7 ~>

------------------------------------------------------------------------------
-- | Query and deletion of elements of a set, using either element value or
--   element index.
--
--   TODO: This is ambiguous in naming. E.g., for a `Vector`, we may want to
--     delete by position and/or by element value? Perhaps rename this class
--     to `SetElement`? The `i` parameter should be `e`?
--   TODO: Do I need separate functions for:
--      + delete by element; and
--      + delete by index/position?
class SetIndex i s | s -> i where
  (<\) :: i -> s -> s     -- Delete
  (<?) :: i -> s -> Bool  -- Member?
  default (<?) :: (i ~ Element s, Eq i, MonoFoldable s) => i -> s -> Bool
  (<?)  = oelem
  {-# INLINE (<?) #-}

-- | Not a member?
(>?) :: SetIndex i s => i -> s -> Bool
(>?) = (not .) . (<?)
{-# INLINE (>?) #-}

infixr 4 <\
infixl 4 <?, >?

------------------------------------------------------------------------------
-- | A family of functions to construct empty and singleton sets.
class SetBuild e s | s -> e where
  bare :: s
  unit :: e -> s
  default unit :: (e ~ Element s, MonoPointed s) => e -> s
  unit  = opoint
  {-# INLINE unit #-}

------------------------------------------------------------------------------
-- | Retrieves the element at the given position/index.
class SetElems i e s | s -> i, s -> e where
  (<~) :: i -> s -> e
  default (<~) :: (Maybe (Element s) ~ e, Index s ~ i, IsSequence s)
               => i -> s -> e
  (<~)  = flip index
  {-# INLINE (<~) #-}

infixl 6 <~

------------------------------------------------------------------------------
-- | Flatten set to a list.
--   FIXME: Duplicates functionality of `MonoTraversable'.
class SetFlat e s | s -> e where
  flat :: s -> [e]
  default flat :: (e ~ Element s, MonoFoldable s) => s -> [e]
  flat  = otoList
  {-# INLINE flat #-}


-- * Helper functions
------------------------------------------------------------------------------
fill :: (Foldable f, SetInsert e s) => f e -> s -> s
fill  = flip $ foldl' (flip (~>))
{-# INLINE fill #-}

filr :: (Foldable f, SetInsert e s) => f e -> s -> s
filr  = flip $ foldr (~>)
{-# INLINE filr #-}

lfil :: (Foldable f, SetInsert e s) => s -> f e -> s
lfil  = foldl' (flip (~>))
{-# INLINE lfil #-}

rfil :: (Foldable f, SetInsert e s) => s -> f e -> s
rfil  = foldr (~>)
{-# INLINE rfil #-}
