# README for `set-extras` #

Some convenience functions for working with sets and maps.

## TODO ##

Use [`summoner`](http://hackage.haskell.org/package/summoner) to generate a new project skeleton, including CI, and host on Github:

```
  sets-handy/
  ├── CHANGELOG.md
  ├── LICENSE
  ├── README.md
  ├── benchmark/
  │   └── Main.hs
  ├── sets-handy.cabal
  ├── src/
  │   └── SetsHandy.hs
  ├── stack.yaml
  └── test/
      └── Spec.hs
```
